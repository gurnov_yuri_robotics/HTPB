//Validator
bool SensorIsWathing (int distanceAtLeft, int distanceAtRight)
{
  const int ThresholdOfDistance = 50;
  
  if (distanceAtLeft < ThresholdOfDistance || distanceAtRight < ThresholdOfDistance)
    return true;
  return false;  
}

bool TwoSensorsAreWathing (int distanceAtLeft, int distanceAtRight)
{
  const int ThresholdOfDistance = 30;
  
  if (distanceAtLeft < ThresholdOfDistance && distanceAtRight < ThresholdOfDistance)
    return true;
  return false; 
}

bool SensorIsValid (int value)
{
  const int ThresholdOfValid = 350;
  
  if (value < ThresholdOfValid && value >= 0)
    return true;
  return false; 
}


