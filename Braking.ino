void Braking ()
{
  const int NORM_POWER_VALUE = static_cast <int> (170 * allPower);
  
  driver.left.ChangePower (-NORM_POWER_VALUE);
  driver.right.ChangePower (-NORM_POWER_VALUE);
  delay (70);

  driver.NoPower();
}

