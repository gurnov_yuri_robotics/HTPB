void AlignInPlace ()
{
  const int NORM_POWER_VALUE = static_cast <int> (180 * allPower);
  
  int distanceAtRight = FindDistanceToLine (2);
  int distanceAtLeft = FindDistanceToLine (1);
  
  while (abs (distanceAtRight - distanceAtLeft) != 0)
  {
    if (distanceAtRight > distanceAtLeft)
    {
      //Serial.println ("distanceAtRight > distanceAtLeft");
      
      
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower (-NORM_POWER_VALUE); //Крутим колесо назад
    
      delay (45);
  
      driver.NoPower();
    }

    else
    {
      //Serial.println ("distanceAtRight < distanceAtLeft");
      
      driver.right.ChangePower (-NORM_POWER_VALUE);
      driver.left.ChangePower (NORM_POWER_VALUE); //Крутим колесо назад
    
      delay (45);
  
      driver.NoPower();
    }

    distanceAtRight = FindDistanceToLine (2);
    distanceAtLeft = FindDistanceToLine (1);
  }
}

