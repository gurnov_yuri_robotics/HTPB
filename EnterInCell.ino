void EnterInCell ()
{
  const int NORM_POWER_VALUE = static_cast <int> (105 * allPower);
  const int sizeOfTurn = 20;
  const int tresholdLackOfLet = 20;
  
  while (TwoSensorsAreWathing (FindDistanceToLine (0), FindDistanceToLine (3)) == false)
  { 
    int distanceAtLeft = FindDistanceToLine (1);
    int distanceAtRight = FindDistanceToLine (2);
    
    if (distanceAtLeft < distanceAtRight)
    {
      driver.left.ChangePower (NORM_POWER_VALUE);
      driver.right.ChangePower (NORM_POWER_VALUE - (distanceAtRight - distanceAtLeft) * sizeOfTurn); //проверено
    }
    
    else
    {
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower (NORM_POWER_VALUE - (distanceAtLeft - distanceAtRight) * sizeOfTurn); //проверено
    }
    
    delay (10);
  }
}


/*{ 
    int distanceAtLeft = FindDistanceToLine (1);
    int distanceAtRight = FindDistanceToLine (2);

    if (abs (distanceAtLeft - distanceAtRight) < tresholdLackOfLet)
      driver.ChangeOfAllPower (NORM_POWER_VALUE);
    
    if (distanceAtLeft < distanceAtRight * 0.8)
    {
      driver.Step('r');
    }
    
    else
    {
      driver.Step('l');
    }
    
    delay (10);
  }*/
