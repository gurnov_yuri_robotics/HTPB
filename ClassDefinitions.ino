#include <Wire.h>
#include "Kalman//Kalman.h"

  inline void diod::ON()
  {
    digitalWrite (pin, HIGH);
  }

  inline void diod::OFF()
  {
    digitalWrite (pin, LOW);
  }


  int Sensor::Read()
  {
    return pulseIn (data, HIGH);
  }

  void Engine::ChangePower(int valueOfPower)
  {
    if (valueOfPower >= 0)
    {
      analogWrite (power, valueOfPower);
      digitalWrite (forward, HIGH);
      digitalWrite (back, LOW);
    }

    else
    {
      analogWrite (power, -valueOfPower);
      digitalWrite (back, HIGH);
      digitalWrite (forward, LOW);
    }
    
  }

  void Engine::FullPower()
  {
    digitalWrite (forward, HIGH);
    digitalWrite (back, LOW);
    digitalWrite (power, HIGH);
  }

  void Engine::FullPowerBack()
  {
    digitalWrite (power, HIGH);
    digitalWrite (back, HIGH);
    digitalWrite (forward, LOW);
  }

  void Engine::NoPower()
  {
    digitalWrite (power, LOW);
  }

  void DoubleEngine::ChangePower(int power)
  {
    front.ChangePower(power);
    rearward.ChangePower(power);
  }

  void DoubleEngine::FullPower()
  {
    front.FullPower();
    rearward.FullPower();
  }

  void DoubleEngine::FullPowerBack()
  {
    front.FullPowerBack();
    rearward.FullPowerBack();
  }

  void DoubleEngine::NoPower()
  {
    front.NoPower();
    rearward.NoPower();
  }


  void TheWholeEngine::NoPower()
  {
    left.NoPower();
    right.NoPower();
  }

  void TheWholeEngine::FullPower()
  {
    left.FullPower();
    right.FullPower();
  }

  void TheWholeEngine::rotateDegree (int numberOfDegree, char directionOfTurn) //2 - вправо
  {
    const float timeOfTurnOnOneDegree = 30.8f;
    float timeOfTurnOneStep = timeOfTurnOnOneDegree * numberOfDegree / 30.0f;

    if (directionOfTurn == 'l')
    {
      int i = 0;
      while (i < 30)
      {
        left.ChangePower (-NORM_POWER_VALUE);
        right.ChangePower (NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (100);

        i++;
      }
    }
    
    else
    {
      int i = 0;
      while (i < 30)
      {
        left.ChangePower (NORM_POWER_VALUE);
        right.ChangePower (-NORM_POWER_VALUE);

        delay (timeOfTurnOneStep);
        
        NoPower();

        delay (100);

        i++;
      }
    }
  }

  void TheWholeEngine::ChangePower (int power, int placementOfLine)
  {
    if (placementOfLine = 0)
      left.ChangePower (power);
    else 
      right.ChangePower (power);
  }

  void TheWholeEngine::ChangeOfAllPower (int power)
  {
    left.ChangePower (power);
    right.ChangePower (power);
  }

  void TheWholeEngine::Step (char directionOfStep)
  {
    const int valueOfStep = 500;
    const float sin45 = 0.72f;
    
    if (directionOfStep == 'r')
    {
      ChangeOfAllPower (-NORM_POWER_VALUE);
      delay (valueOfStep);

      NoPower();

      rotateDegree (45, 'r');

      ChangeOfAllPower (NORM_POWER_VALUE);
      delay (valueOfStep / sin45);

      NoPower();

      rotateDegree (45, 'l');
    }

    else
    {
      ChangeOfAllPower (-NORM_POWER_VALUE);
      delay (valueOfStep);

      NoPower();

      rotateDegree (45, 'l');

      ChangeOfAllPower (NORM_POWER_VALUE);
      delay (valueOfStep / sin45);

      NoPower();

      rotateDegree (45, 'r');
    }
  }

