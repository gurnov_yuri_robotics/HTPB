///////////////////////////////////////////////////
//Линия справа

void DriveInEmptyCellAtRight ()
{  
  standNextToLineRight ();

  DriveUpToPlaceOfParkingRight ();
  
  DriveInCellRight ();

  driver.NoPower();
}

void standNextToLineRight ()
{
  const int valueOfPowerRight = -static_cast <int> (255 * allPower);
  const int valueOfPowerLeft = static_cast <int> (255 * allPower);

  PullUpToLineRight (valueOfPowerLeft, valueOfPowerRight);

  driver.NoPower();
  
  DriveUpToLine();
  driver.rotateDegree (90, 'l');
} 

void PullUpToLineRight (int leftEnginePower, int rightEnginePower) 
{
  int distanceAtRight = FindDistanceToLine (2);
  int distanceAtLeft = FindDistanceToLine (1);

  if (TwoSensorsAreWathing (distanceAtLeft, distanceAtRight) == false)
  {
    distanceAtRight = 0;
    distanceAtLeft = 1;
  }
  
  while (distanceAtRight <= distanceAtLeft - 1)
  
  {
    driver.right.ChangePower (rightEnginePower);
    driver.left.ChangePower (leftEnginePower); //Крутим колесо назад

    headlights.right.ON();
    
    delay (100);

    headlights.right.OFF();

    driver.NoPower();

    distanceAtRight = FindDistanceToLine (2);
    distanceAtLeft = FindDistanceToLine (1);
  
    if (TwoSensorsAreWathing (distanceAtLeft, distanceAtRight) == false)
    {
      distanceAtRight = 0;
      distanceAtLeft = 1;
    }

  }
}

void DriveUpToPlaceOfParkingRight ()
{
  const int distanceToLine = FindDistanceToLine (3);
  
  bool thereIsSuitableCell = false;

  while (thereIsSuitableCell == false)
  {
    DriveWithSmoothingOfLineRight (distanceToLine);

    if (MeasureSizeOfCellRight (distanceToLine) == true)
        thereIsSuitableCell = true;
  }
}

void DriveWithSmoothingOfLineRight (int distanceToLine)
{
  const int NORM_POWER_VALUE = static_cast <int> (100 * allPower);
  const int sizeOfTurn = 40;
  const float maxLineCoeff = 1.4f;
  
  int recentDistance = FindDistanceToLine (3);
  
  while (recentDistance < maxLineCoeff * distanceToLine)
  {
    //driver.ChangeOfAllPower(MAX_POWER_VALUE);
    
    if (recentDistance < distanceToLine)
    {
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower (NORM_POWER_VALUE - (distanceToLine - recentDistance) * sizeOfTurn); //проверено
    }
    
    else
    {
      driver.left.ChangePower (NORM_POWER_VALUE);
      driver.right.ChangePower (NORM_POWER_VALUE - (recentDistance - distanceToLine) * sizeOfTurn); //проверено
    }
    
    delay (10);
    recentDistance = FindDistanceToLine (3);
    
  }

  Braking();
      
}

bool MeasureSizeOfCellRight (int distanceToLine) //check
{
  const float timeOfPassageOfCell = 0.54f;
  const int NORM_POWER_VALUE = static_cast <int> (80 * allPower);
  
  driver.ChangeOfAllPower(NORM_POWER_VALUE);
  
  delay (timeOfPassageOfCell * 1000);
  
  if ( FindDistanceToLine(3) > 1.1 * distanceToLine)
    return true;
  return false;
}

void DriveInCellRight ()
{
  driver.rotateDegree (90, 'r'); //Right 

  EnterInCell ();

  AlignInCell ();

  AlignInPlace ();
}
