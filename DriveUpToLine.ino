void DriveUpToLine () //check
{
  const int minDistanceToLine = 23;
  driver.ChangeOfAllPower(90);
  
  while (FindDistanceToLine (1) >= minDistanceToLine && FindDistanceToLine (2) >= minDistanceToLine)
    delay (100);

  driver.NoPower();
}
