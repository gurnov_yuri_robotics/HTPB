void AlignInCell ()
{
  const int NORM_POWER_VALUE = static_cast <int> (81 * allPower);
  const int sizeOfTurn = 23;
  const int MinDistanceAtFront = 15;

  driver.ChangeOfAllPower(NORM_POWER_VALUE);
  delay(100);
  
  while (FindDistanceToLine (1) > MinDistanceAtFront && FindDistanceToLine (2) > MinDistanceAtFront)
  {
    int distanceAtLeft = FindDistanceToLine (0);
    int distanceAtRight = FindDistanceToLine (3);
    
    if (distanceAtLeft < distanceAtRight)
    {
      driver.left.ChangePower (NORM_POWER_VALUE);
      driver.right.ChangePower (NORM_POWER_VALUE - (distanceAtRight - distanceAtLeft) * sizeOfTurn); //проверено
    }
    
    else
    {
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower (NORM_POWER_VALUE - (distanceAtLeft - distanceAtRight) * sizeOfTurn); //проверено
    }
    
    delay (10);
  }

  Braking();
}


