//////////////////////////////////////////////////
//Линия слева

void DriveInEmptyCellAtLeft ()
{
  standNextToLineLeft ();

  DriveUpToPlaceOfParkingLeft ();

  DriveInCellLeft ();

  driver.NoPower();
}

void standNextToLineLeft ()
{
  const int valueOfPowerRight = static_cast <int> (230 * allPower);
  const int valueOfPowerLeft = -static_cast <int> (230 * allPower);
  
  PullUpToLineLeft (valueOfPowerLeft, valueOfPowerRight);

  driver.NoPower();
  
  DriveUpToLine();
  driver.rotateDegree (90, 'r');
}

void PullUpToLineLeft (int leftEnginePower, int rightEnginePower)
{
  int distanceAtRight = FindDistanceToLine (2);
  int distanceAtLeft = FindDistanceToLine (1);

  if (TwoSensorsAreWathing (distanceAtLeft, distanceAtRight) == false)
  {
    distanceAtRight = 1;
    distanceAtLeft = 0;
  }
  
  while (distanceAtLeft <= distanceAtRight - 1)
  
  {
    driver.right.ChangePower (rightEnginePower);
    driver.left.ChangePower (leftEnginePower); //Крутим колесо назад
    
    delay (80);

    driver.NoPower();

    distanceAtRight = FindDistanceToLine (2);
    distanceAtLeft = FindDistanceToLine (1);
  
    if (TwoSensorsAreWathing (distanceAtLeft, distanceAtRight) == false)
    {
      distanceAtRight = 1;
      distanceAtLeft = 0;
    }
      
  }
}

void DriveUpToPlaceOfParkingLeft ()
{  
  const int distanceToLine = FindDistanceToLine (0);
  
  bool thereIsSuitableCell = false;

  while (thereIsSuitableCell == false)
  {
    DriveWithSmoothingOfLineLeft (distanceToLine);

    if (MeasureSizeOfCellLeft (distanceToLine) == true)
        thereIsSuitableCell = true;
  }
}

void DriveWithSmoothingOfLineLeft (int distanceToLine)
{
  const int NORM_POWER_VALUE = static_cast <int> (100 * allPower);
  const int sizeOfTurn = 40;
  const float maxLineCoeff = 1.4f;
  
  int recentDistance = FindDistanceToLine (0);
  
  while (recentDistance < maxLineCoeff * distanceToLine)
  {
    //driver.ChangeOfAllPower(MAX_POWER_VALUE); 
    
    if (recentDistance < distanceToLine)
    {
      driver.left.ChangePower (NORM_POWER_VALUE);
      driver.right.ChangePower (NORM_POWER_VALUE - (distanceToLine - recentDistance) * sizeOfTurn); //проверено
    }
    
    else
    { 
      driver.right.ChangePower (NORM_POWER_VALUE);
      driver.left.ChangePower (NORM_POWER_VALUE - (recentDistance - distanceToLine) * sizeOfTurn); //проверено
    }
    
    delay (10);
    recentDistance = FindDistanceToLine (0);
    
  }

  Braking ();
}

bool MeasureSizeOfCellLeft (int distanceToLine) //check
{
  const float timeOfPassageOfCell = 0.54f;
  const int NORM_POWER_VALUE = static_cast <int> (80 * allPower);
  
  driver.ChangeOfAllPower(NORM_POWER_VALUE);
  
  delay (timeOfPassageOfCell * 1000);
  
  if ( FindDistanceToLine(0) > 1.1 * distanceToLine)
    return true;
  return false;
}

void DriveInCellLeft ()
{
  driver.rotateDegree (90, 'l'); //Left 

  EnterInCell ();

  AlignInCell ();

  AlignInPlace ();
}
