int TakeDistanceBeforeObject (int i) //check
{
  digitalWrite (ultraSoung [i].output, HIGH);
  delayMicroseconds(10);
  digitalWrite (ultraSoung [i].output, LOW);
  
  int impulseTime = ultraSoung [i].Read();
  
  return impulseTime / 58;
}

int FindDistanceToLine (int number) //check возвращает среднее значение расстояния до линии
{
  int sample = 0;
  
  for (int i = 0; i < 4; ++i)
  {
    int cache = TakeDistanceBeforeObject (number);

    while (SensorIsValid (cache) == false)
    {
      cache = TakeDistanceBeforeObject (number);
    }
    
    sample += cache;
    delay (10);
  }
  return sample / 4;
}
