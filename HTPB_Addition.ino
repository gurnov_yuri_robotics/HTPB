struct diod
{
  int pin;

  inline void ON();

  inline void OFF();
};

struct headlights
{
  diod right;

  diod left;
};

struct Sensor
{
    int data;
    int output;
    
    int Read();
};

struct Engine
{
    const int NORM_POWER_VALUE = 255;
  
    int forward;
    int back;
    int power;

    void ChangePower(int valueOfPower);

    void FullPower();
    void FullPowerBack();

    void NoPower();
};

struct DoubleEngine
{
    Engine rearward;
    Engine front;

    void ChangePower(int power);

    void FullPower();
    void FullPowerBack();

    void NoPower();
};

struct TheWholeEngine
{
    const int NORM_POWER_VALUE = 255;
  
    DoubleEngine left;
    DoubleEngine right;

    void NoPower();

    void FullPower();

    void rotateDegree (int numberOfDegree, char directionOfTurn); //2 - вправо

    void ChangePower (int power, int placementOfLine);

    void ChangeOfAllPower (int power);

    void Step (char directionOfStep);
};

headlights headlights;

Sensor ultraSoung [4];
int checkingDiod = 42;

TheWholeEngine driver;

void setup() {
  
  driver.left.rearward.forward = 11;
  driver.left.rearward.back = 12;
  driver.left.rearward.power = 13; 

  driver.left.front.power = 5;
  driver.left.front.forward = 6;
  driver.left.front.back = 7;

  driver.right.rearward.power = 8;
  driver.right.rearward.forward = 10;
  driver.right.rearward.back = 9;

  driver.right.front.power = 2;
  driver.right.front.forward = 4;
  driver.right.front.back = 3;
  
  pinMode (driver.left.rearward.power, OUTPUT);
  pinMode (driver.left.rearward.forward, OUTPUT);
  pinMode (driver.left.rearward.back, OUTPUT);

  pinMode (driver.left.front.power, OUTPUT);
  pinMode (driver.left.front.forward, OUTPUT);
  pinMode (driver.left.front.back, OUTPUT);

  pinMode (driver.right.rearward.power, OUTPUT);
  pinMode (driver.right.rearward.forward, OUTPUT);
  pinMode (driver.right.rearward.back, OUTPUT);

  pinMode (driver.right.front.power, OUTPUT);
  pinMode (driver.right.front.forward, OUTPUT);
  pinMode (driver.right.front.back, OUTPUT);

  ultraSoung [0].data = 33;
  ultraSoung [0].output = 32;

  ultraSoung [1].data = 36;
  ultraSoung [1].output = 37;


  ultraSoung [2].data = 29;
  ultraSoung [2].output = 28;

  ultraSoung [3].data = 38;
  ultraSoung [3].output = 39;
  
  pinMode (ultraSoung [0].data, INPUT);
  pinMode (ultraSoung [1].data, INPUT);
  pinMode (ultraSoung [2].data, INPUT);
  pinMode (ultraSoung [3].data, INPUT);

  pinMode (ultraSoung [0].output, OUTPUT);
  pinMode (ultraSoung [1].output, OUTPUT);
  pinMode (ultraSoung [2].output, OUTPUT);
  pinMode (ultraSoung [3].output, OUTPUT);

  headlights.right.pin = 51;
  headlights.left.pin = 50;

  pinMode (headlights.right.pin, INPUT);
  pinMode (headlights.left.pin, INPUT);

  Serial.begin (9600); 

  pinMode (checkingDiod, OUTPUT);
}

///////////////////////////////////////////////
const float allPower = 2.3f;
///////////////////////////////////////////////

static bool theEnd = false;

void loop() //Check
{ 
  if (theEnd == false)
  {
    if (LineAtRight () == true)
    {    
       DriveInEmptyCellAtRight ();
    }
    
    else 
    {      
      DriveInEmptyCellAtLeft ();
    }
  }  

  HiteTheDiod();

  theEnd = true;

}
